import { ApolloServer } from 'apollo-server';
import { typeDefs, resolvers } from './modules/graphql';
import db from './modules/database';

const start = async () => {
  await db;

  const server = new ApolloServer({
    typeDefs,
    resolvers,
  });

  const { url } = await server.listen();

  console.log(`🚀 Server ready at ${url}`);
};

start();
