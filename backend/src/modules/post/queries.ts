import Author from '../author';
import Post from './model';
import { QueryResolvers } from '../../generated/types';

const postQueries: QueryResolvers = {
  post: async (_parent, { id }, _context) => {
    const post = await Post.findByPk(id, {
      include: [{ model: Author }],
    });

    if (!post) {
      throw new Error('post not found');
    }

    return post;
  },
};

export default postQueries;
