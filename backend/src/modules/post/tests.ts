import { createTestClient } from 'apollo-server-testing';
import { gql } from 'apollo-server';
import createTestServer from '../tests';
import db from '../database';

import { Mutation, PostStatus } from '../../generated/types';

const CREATE_TEST_AUTHOR = gql`
  mutation {
    createAuthor(name: "test-author") {
      id
      name
    }
  }
`;

const POST = gql`
  query($id: Int!) {
    post(id: $id) {
      id
      title
      thumbnailUrl
      content
      status
      authorId
      author {
        name
      }
    }
  }
`;

const CREATE_POST = gql`
  mutation($title: String!, $thumbnailUrl: String, $content: String!, $status: PostStatus!, $authorId: Int!) {
    createPost(title: $title, thumbnailUrl: $thumbnailUrl, content: $content, status: $status, authorId: $authorId) {
      id
      title
      thumbnailUrl
      content
      status
      authorId
      author {
        name
      }
    }
  }
`;

const server = createTestServer();
const { query, mutate } = createTestClient(server);

beforeAll(async done => {
  await db;
  done();
});

let postId = 0;
describe('Mutations', () => {
  let testAuthorId = 0;

  it('should create a test author', async () => {
    const response = await mutate({
      mutation: CREATE_TEST_AUTHOR,
    });
    if (response.errors) {
      throw response.errors;
    }

    testAuthorId = (response.data as Mutation).createAuthor.id;

    expect(response).toMatchSnapshot();
  });

  it('should create a post', async () => {
    await mutate({
      mutation: CREATE_TEST_AUTHOR,
    });

    const response = await mutate({
      mutation: CREATE_POST,
      variables: {
        title: 'test-post',
        thumbnailUrl: 'test-thumbnail-url',
        content: 'test-content',
        status: PostStatus.Published,
        authorId: testAuthorId,
      },
    });
    if (response.errors) {
      throw response.errors;
    }

    postId = (response.data as Mutation).createPost.id;

    expect(response).toMatchSnapshot();
  });
});

describe('Queries', () => {
  it('should find a post', async () => {
    const response = await query({ query: POST, variables: { id: postId } });

    expect(response).toMatchSnapshot();
  });

  it('should not find a post', async () => {
    const response = await query({ query: POST, variables: { id: 0 } });

    expect(response).toMatchSnapshot();
  });
});
