import { DataType } from 'sequelize-typescript';
import { ModelAttributeColumnOptions, literal } from 'sequelize';

export const createdAtOptions: Partial<ModelAttributeColumnOptions> = {
  type: DataType.DATE,
  allowNull: false,
  defaultValue: literal('CURRENT_TIMESTAMP'),
};

export const updatedAtOptions: Partial<ModelAttributeColumnOptions> = {
  type: DataType.DATE,
  allowNull: false,
  defaultValue: literal('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'),
};
