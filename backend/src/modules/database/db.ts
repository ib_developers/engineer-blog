import { Sequelize } from 'sequelize-typescript';
import models from './models';
import { MYSQL_HOST, MYSQL_DATABASE, MYSQL_USER, MYSQL_PASSWORD, NODE_ENV } from '../settings';

const sequelize = new Sequelize({
  host: MYSQL_HOST,
  database: MYSQL_DATABASE,
  username: MYSQL_USER,
  password: MYSQL_PASSWORD,
  dialect: NODE_ENV === 'testing' ? 'sqlite' : 'mysql',
  logging: NODE_ENV === 'development' ? console.log : undefined,
});

const db = models(sequelize);

export default db;
