import {
  Table,
  Column,
  Model,
  DataType,
  AllowNull,
  HasMany,
  CreatedAt,
  UpdatedAt,
  PrimaryKey,
  AutoIncrement,
} from 'sequelize-typescript';

import { createdAtOptions, updatedAtOptions } from '../column-option/date';
import Post from '../post';

@Table
export default class Author extends Model<Author> {
  @PrimaryKey
  @AutoIncrement
  @Column
  public id!: number;

  @AllowNull(false)
  @Column(DataType.STRING)
  public name!: string;

  @HasMany(() => Post)
  public posts!: Post[];

  @CreatedAt
  @Column(createdAtOptions)
  public createdAt!: Date;

  @UpdatedAt
  @Column(updatedAtOptions)
  public updatedAt!: Date;
}
