import { gql } from 'apollo-server';

const authorTypeDefs = gql`
  extend type Query {
    author(id: Int!): Author!
  }

  extend type Mutation {
    createAuthor(name: String!): Author!
  }

  type Author {
    id: Int!
    name: String!
    posts: [Post!]!
    createdAt: DateTime!
    updatedAt: DateTime!
  }
`;

export default authorTypeDefs;
