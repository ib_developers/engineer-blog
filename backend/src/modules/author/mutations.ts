import Author from './model';
import { MutationResolvers } from '../../generated/types';

const authorMutations: MutationResolvers = {
  createAuthor: async (_parent, { name }, _context) => {
    const author = await Author.create({
      name,
    });

    return author;
  },
};

export default authorMutations;
