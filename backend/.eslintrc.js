module.exports = {
  root: true,
  parser: '@typescript-eslint/parser',
  extends: [
    'plugin:@typescript-eslint/recommended',
    'airbnb-base',
    'plugin:import/errors',
    'plugin:import/warnings',
    'plugin:import/typescript',
    'plugin:jest/recommended',
    'prettier/@typescript-eslint',
    'plugin:prettier/recommended',
  ],
  parserOptions: {
    ecmaVersion: 2018,
    sourceType: 'module',
    warnOnUnsupportedTypeScriptVersion: false,
  },
  rules: {
    // タイプでreturn typeすでに指定してる
    '@typescript-eslint/explicit-function-return-type': 0,
    // _で始まる変数を無視する
    '@typescript-eslint/no-unused-vars': [2, { argsIgnorePattern: '^_'}],
    // テストファイルでapollo-server-testingをインポートするため
    'import/no-extraneous-dependencies': [2, { devDependencies: ['**/tests.ts'] }],
    // @typescript-eslint/no-unused-varsで対応
    'no-unused-vars': 0,
    // TODO: これを解決して（モデルの定義で循環参照が発生するので、eslintによるチェックを無効にしています）
    'import/no-cycle': 0,
    // テストファイルでapollo-server-testingをインポートするため
    'import/no-extraneous-dependencies': [2, { 'devDependencies': ['**/tests.ts'] }]
  },
  settings: {
    'import/resolver': {
      node: {
        extensions: ['.js', '.ts'],
      },
    },
  },
};
