module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  testRegex: 'src/.*tests.ts$',
  snapshotResolver: './src/modules/tests/snapshotResolver.js',
};
