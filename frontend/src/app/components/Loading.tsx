import React from 'react';

const Loading: React.FunctionComponent = () => <p>読み込み中...</p>;

export default Loading;
