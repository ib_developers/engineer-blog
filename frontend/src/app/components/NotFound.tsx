import React from 'react';

const NotFound: React.FunctionComponent = () => <p>該当するページは存在しません。</p>;

export default NotFound;
