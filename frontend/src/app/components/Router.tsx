import React, { Suspense, lazy } from 'react';
import { Route, BrowserRouter, Switch } from 'react-router-dom';
import Loading from './Loading';

const Posts = lazy(() => import(/* webpackChunkName: "posts" */ '../../modules/posts/components/PostList'));
const NotFound = lazy(() => import(/* webpackChunkName: "notFound" */ './NotFound'));

const Router: React.FunctionComponent = () => (
  <BrowserRouter>
    <Suspense fallback={<Loading />}>
      <Switch>
        <Route path="/" exact>
          <Posts />
        </Route>
        <Route>
          <NotFound />
        </Route>
      </Switch>
    </Suspense>
  </BrowserRouter>
);

export default Router;
