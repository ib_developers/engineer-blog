import React from 'react';
import styled from 'styled-components';
import Badge from './Badge';
import Header from './Header';
import Thumbnail from './Thumbnail';
import TableOfContents from './TableOfContents';
import Content from './Content';
import H1 from './H1';

const Article = styled.article`
  max-width: ${props => props.theme.contentWidth};
  padding-right: 1rem;
  padding-left: 1rem;
  margin-right: auto;
  margin-left: auto;
`;

const Post: React.FunctionComponent = () => {
  return (
    <>
      <Article>
        <Badge />
        <H1>setroubled による SELinux の運用</H1>
        <Header />
        <Thumbnail />
        <TableOfContents />
        <Content />
      </Article>
    </>
  );
};
export default Post;
