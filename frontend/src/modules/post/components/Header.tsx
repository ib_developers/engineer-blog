import React from 'react';
import styled from 'styled-components';

const Div = styled.div`
  margin-bottom: 1rem;
`;

const Span = styled.span`
  margin-right: 1rem;
`;

const PostHeader: React.FunctionComponent = () => {
  return (
    <>
      <Div>
        <Span>2019-07-31</Span>
        <Span>秦&emsp;友幸</Span>
      </Div>
    </>
  );
};

export default PostHeader;
