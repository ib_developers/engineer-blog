import styled from 'styled-components';

const H1 = styled.h1`
  margin-bottom: 1rem;
`;

export default H1;
