import React from 'react';
import styled from 'styled-components';
import 'code-prettify/loader/run_prettify';
import 'code-prettify/loader/lang-vhdl';
import 'code-prettify/loader/skins/desert.css';

const Div = styled.div`
  max-width: 780px;
  padding-top: 2rem;
  margin-right: auto;
  margin-left: auto;
`;

const PrettyPrint = styled.pre`
  margin-bottom: 2rem;
  overflow: auto;

  ol.linenums > li {
    margin-left: 40px;
    list-style-type: decimal;

    &:nth-child(even) {
      background-color: #444;
    }
  }
`;

const Img = styled.img`
  display: block;
  max-width: 100%;
  height: auto;
  margin-right: auto;
  margin-bottom: 2rem;
  margin-left: auto;
`;

const P = styled.p`
  margin-bottom: 2rem;
`;

const PostContent: React.FunctionComponent = () => {
  return (
    <>
      <Div>
        <P>
          親譲りの無鉄砲で小供の時から損ばかりしている。小学校に居る時分学校の二階から飛び降りて一週間ほど腰を抜かした事がある。
          なぜそんな無闇をしたと聞く人があるかも知れぬ。別段深い理由でもない。
          新築の二階から首を出していたら、同級生の一人が冗談に、いくら威張っても、そこから飛び降りる事は出来まい。
        </P>
        <Img src="/images/publicdomainq-0036948ksahje.jpg" alt="パブリックドメインな猫" />
        <P>
          弱虫やーい。と囃したからである。
          小使に負ぶさって帰って来た時、おやじが大きな眼をして二階ぐらいから飛び降りて腰を抜かす奴があるかと云ったから、この次は抜かさずに飛んで見せますと答えた。（青空文庫より）
        </P>
        <PrettyPrint className="prettyprint lang-vhdl linenums">
          {`-- PPU (Picture Processing Unit)
process(clk) begin
  if (rising_edge(clk)) then
    case ppu_clkcnt is
      when 2032 =>;
        ppu_clkcnt <= 0;
        
        case ppu_linecnt is
          when 262 =>;
            ppu_linecnt <= 0;
          when others =>
            ppu_linecnt <= ppu_linecnt + 1;
        end case;
        
      when others =>
        ppu_clkcnt <= ppu_clkcnt + 1;
    end case;
  end if;
end process;

stat_vsync <= '1' when ppu_linecnt<3 else '0';
stat_hsync <= '1' when ppu_clkcnt<150 else '0';

lum <= crom_1_dot;
`}
        </PrettyPrint>
      </Div>
    </>
  );
};

export default PostContent;
