/* eslint-disable @typescript-eslint/no-var-requires */
const merge = require('webpack-merge');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpack = require('webpack');
const common = require('./webpack.common.js');

module.exports = merge(common, {
  mode: 'development',
  output: {
    filename: 'runtime.js',
    path: `${__dirname}/public/static/js`,
    publicPath: '/static/js/',
    chunkFilename: '[name].js',
  },
  plugins: [
    new HtmlWebpackPlugin({
      filename: `${__dirname}/public/index.html`,
      template: './src/index.html',
    }),
    new webpack.HotModuleReplacementPlugin(),
  ],
  devServer: {
    hot: true,
    host: '0.0.0.0',
    contentBase: `${__dirname}/public/`,
    overlay: true,
    historyApiFallback: true,
    disableHostCheck: true,
  },
});
