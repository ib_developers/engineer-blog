module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'jsdom',
  testRegex: 'src/.*.tests.tsx$',
  snapshotResolver: './snapshotResolver.js',
};
