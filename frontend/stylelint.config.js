module.exports = {
  processors: 'stylelint-processor-styled-components',
  extends: ['stylelint-config-standard', 'stylelint-config-standard-scss', 'stylelint-config-recess-order'],
  plugins: ['stylelint-no-unsupported-browser-features', 'stylelint-declaration-block-no-ignored-properties'],
  rules: {
    'no-descending-specificity': null,
    // CSSが書いていない.tsxファイルがあるため
    'no-empty-source': null,
    'plugin/no-unsupported-browser-features': true,
    'plugin/declaration-block-no-ignored-properties': true,
  },
};
