module.exports = {
  root: true,
  env: {
    browser: true,
  },
  parser: '@typescript-eslint/parser',
  extends: [
    'plugin:@typescript-eslint/recommended',
    'airbnb',
    'plugin:import/errors',
    'plugin:import/warnings',
    'plugin:import/typescript',
    'plugin:react/recommended',
    'plugin:jest/recommended',
    'prettier/@typescript-eslint',
    'plugin:prettier/recommended',
  ],
  parserOptions: {
    ecmaVersion: 2018,
    ecmaFeatures: {
      jsx: true,
    },
    sourceType: 'module',
    warnOnUnsupportedTypeScriptVersion: false,
  },
  rules: {
    // タイプでreturn typeすでに指定してる
    '@typescript-eslint/explicit-function-return-type': 0,
    // _で始まる変数を無視する
    '@typescript-eslint/no-unused-vars': [2, { argsIgnorePattern: '^_' }],
    // @typescript-eslint/camelcaseで対応
    camelcase: 0,
    // accessibility系は不要
    'jsx-a11y/no-static-element-interactions': 0,
    'jsx-a11y/click-events-have-key-events': 0,
    // @typescript-eslint/no-unused-varsで対応
    'no-unused-vars': 0,
    // tsxを対応する
    'react/jsx-filename-extension': [1, { extensions: ['.jsx', '.tsx'] }],
    // Typescriptで対応
    'react/prop-types': 0,
  },
  settings: {
    'import/resolver': {
      node: {
        extensions: ['.js', '.ts', '.tsx'],
      },
    },
    react: {
      version: 'detect',
    },
  },
};
